import Vue from 'vue'
import App from './App.vue'

import { Form, Filter, TodoList } from './components';

Vue.component('m-form', Form);
Vue.component('m-filter', Filter);
Vue.component('m-todolist', TodoList);


new Vue({
  el: '#app',
  render: h => h(App),
})
